function homeAnimate() {
  var that = this;
  var wWidth, wHeight, flagDevice = 1;
  var pathDash = $('.dashed'),
    durationObj = [];
  var controller;
  var sceneLine1, sceneLine2, sceneLine3, sceneLine4, sceneLine5, sceneLine6, sceneLine7, sceneLine8, sceneLine9, sceneLine10, sceneLine11, sceneLine12;

  var tlObj1 = {
      lineLength: 10780,
      curentlineLength: 10780,
      tolineLength: 8500,
      time: 3
    },
    tlObj2 = {
      lineLength: 10780,
      curentlineLength: 8500,
      tolineLength: 7160,
      time: 3
    },
    tlObj3 = {
      lineLength: 10780,
      curentlineLength: 7160,
      tolineLength: 5615,
      time: 3
    },
    tlObj4 = {
      lineLength: 10780,
      curentlineLength: 7160,
      tolineLength: 5615,
      time: 3
    },
    tlObj5 = {
      lineLength: 10780,
      curentlineLength: 8500,
      tolineLength: 7160,
      time: 3
    },
    tlObj6 = {
      lineLength: 10780,
      curentlineLength: 7160,
      tolineLength: 5615,
      time: 3
    },
    tlObj7 = {
      lineLength: 10780,
      curentlineLength: 7160,
      tolineLength: 5615,
      time: 3
    },
    tlObj8 = {
      lineLength: 10780,
      curentlineLength: 8500,
      tolineLength: 7160,
      time: 3
    },
    tlObj9 = {
      lineLength: 10780,
      curentlineLength: 7160,
      tolineLength: 5615,
      time: 3
    },
    tlObj10 = {
      lineLength: 10780,
      curentlineLength: 8500,
      tolineLength: 7160,
      time: 3
    },
    tlObj11 = {
      lineLength: 10780,
      curentlineLength: 7160,
      tolineLength: 5615,
      time: 3
    },
    tlObj12 = {
      lineLength: 10780,
      curentlineLength: 7160,
      tolineLength: 5615,
      time: 3
    };

  var ele_trigger_0 = '#landing-content',
    ele_trigger_1 = '#landing-article-1 .triggerAnimate',
    ele_trigger_2 = '#landing-article-2 .triggerAnimate',
    ele_trigger_3 = '#landing-article-3 .triggerAnimate',
    ele_trigger_4 = '#landing-article-4 .triggerAnimate',
    ele_trigger_5 = '#landing-article-5 .triggerAnimate',
    ele_trigger_6 = '#landing-article-6 .triggerAnimate',
    ele_trigger_7 = '#landing-article-7 .triggerAnimate',
    ele_trigger_8 = '#landing-article-8 .triggerAnimate',
    ele_trigger_9 = '#landing-article-9 .triggerAnimate',
    ele_trigger_10 = '#landing-article-10 .triggerAnimate',
    ele_trigger_11 = '#landing-article-11 .triggerAnimate';
    ele_trigger_12 = '#landing-article-12 .triggerAnimate';

  var ele_1 = '#landing-article-1 .img',
    ele_2 = '#landing-article-1 .title .main-title',
    ele_title_2 = '#landing-article-1 .title .title-content',
    ele_sub_title_2 = '#landing-article-1 .title .sub-content',
    ele_3 = '#landing-article-1 .content',
    ele_4 = '#landing-article-1 .view-detail';

  var ele_2_1 = '#landing-article-2 .img',
    ele_2_2 = '#landing-article-2 .title .main-title',
    ele_title_2_2 = '#landing-article-2 .title .title-content',
    ele_sub_title_2_2 = '#landing-article-2 .title .sub-content',
    ele_2_3 = '#landing-article-2 .content',
    ele_2_4 = '#landing-article-2 .view-detail';

  var ele_3_1 = '#landing-article-3 .img',
    ele_3_2 = '#landing-article-3 .title .main-title',
    ele_title_3_2 = '#landing-article-3 .title .title-content',
    ele_sub_title_3_2 = '#landing-article-3 .title .sub-content',
    ele_3_3 = '#landing-article-3 .content',
    ele_3_4 = '#landing-article-3 .view-detail';

  var ele_4_1 = '#landing-article-4 .img',
    ele_4_2 = '#landing-article-4 .title .main-title',
    ele_title_4_2 = '#landing-article-4 .title .title-content',
    ele_sub_title_4_2 = '#landing-article-4 .title .sub-content',
    ele_4_3 = '#landing-article-4 .content',
    ele_4_4 = '#landing-article-4 .view-detail';

  var ele_5_1 = '#landing-article-5 .img',
    ele_5_2 = '#landing-article-5 .title .main-title',
    ele_title_5_2 = '#landing-article-5 .title .title-content',
    ele_sub_title_5_2 = '#landing-article-5 .title .sub-content',
    ele_5_3 = '#landing-article-5 .content',
    ele_5_4 = '#landing-article-5 .view-detail';

  var ele_6_1 = '#landing-article-6 .img',
    ele_6_2 = '#landing-article-6 .title .main-title',
    ele_title_6_2 = '#landing-article-6 .title .title-content',
    ele_sub_title_6_2 = '#landing-article-6 .title .sub-content',
    ele_6_3 = '#landing-article-6 .content',
    ele_6_4 = '#landing-article-6 .view-detail';

  var ele_7_1 = '#landing-article-7 .img',
    ele_7_2 = '#landing-article-7 .title .main-title',
    ele_title_7_2 = '#landing-article-7 .title .title-content',
    ele_sub_title_7_2 = '#landing-article-7 .title .sub-content',
    ele_7_3 = '#landing-article-7 .content',
    ele_7_4 = '#landing-article-7 .view-detail';

  var ele_8_1 = '#landing-article-8 .img',
    ele_8_2 = '#landing-article-8 .title .main-title',
    ele_title_8_2 = '#landing-article-8 .title .title-content',
    ele_sub_title_8_2 = '#landing-article-8 .title .sub-content',
    ele_8_3 = '#landing-article-8 .content',
    ele_8_4 = '#landing-article-8 .view-detail';

  var ele_9_1 = '#landing-article-9 .img',
    ele_9_2 = '#landing-article-9 .title .main-title',
    ele_title_9_2 = '#landing-article-9 .title .title-content',
    ele_sub_title_9_2 = '#landing-article-9 .title .sub-content',
    ele_9_3 = '#landing-article-9 .content',
    ele_9_4 = '#landing-article-9 .view-detail';

  var ele_10_1 = '#landing-article-10 .img',
    ele_10_2 = '#landing-article-10 .title .main-title',
    ele_title_10_2 = '#landing-article-10 .title .title-content',
    ele_sub_title_10_2 = '#landing-article-10 .title .sub-content',
    ele_10_3 = '#landing-article-10 .content',
    ele_10_4 = '#landing-article-10 .view-detail';

  var ele_11_1 = '#landing-article-11 .img',
    ele_11_2 = '#landing-article-11 .title .main-title',
    ele_title_11_2 = '#landing-article-11 .title .title-content',
    ele_sub_title_11_2 = '#landing-article-11 .title .sub-content',
    ele_11_3 = '#landing-article-11 .content',
    ele_11_4 = '#landing-article-11 .view-detail';

  var ele_12_1 = '#landing-article-12 .img',
    ele_12_2 = '#landing-article-12 .title .main-title',
    ele_title_12_2 = '#landing-article-12 .title .title-content',
    ele_sub_title_12_2 = '#landing-article-12 .title .sub-content',
    ele_12_3 = '#landing-article-12 .content',
    ele_12_4 = '#landing-article-12 .view-detail';

  var obj_ant = [
    {opacity: 0},
    {opacity: 1, ease: Sine.easeOut},
    {opacity: 1, ease: Back.easeOut},
    {opacity: 0, x: 100},
    {opacity: 1, x: 0, ease: Quint.easeOut}
  ];

  this.animateLine = function () {
    sceneLine1 = new ScrollMagic.Scene({triggerElement: ele_trigger_0, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj1, e.progress.toFixed(3));
      });

    sceneLine2 = new ScrollMagic.Scene({triggerElement: ele_trigger_2, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj2, e.progress.toFixed(3));
      });

    sceneLine3 = new ScrollMagic.Scene({triggerElement: ele_trigger_3, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj3, e.progress.toFixed(3));
      });

    sceneLine4 = new ScrollMagic.Scene({triggerElement: ele_trigger_4, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj4, e.progress.toFixed(3));
      });

    sceneLine5 = new ScrollMagic.Scene({triggerElement: ele_trigger_5, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj5, e.progress.toFixed(3));
      });

    sceneLine6 = new ScrollMagic.Scene({triggerElement: ele_trigger_6, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj6, e.progress.toFixed(3));
      });

    sceneLine7 = new ScrollMagic.Scene({triggerElement: ele_trigger_7, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj7, e.progress.toFixed(3));
      });

    sceneLine8 = new ScrollMagic.Scene({triggerElement: ele_trigger_8, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj8, e.progress.toFixed(3));
      });

    sceneLine9 = new ScrollMagic.Scene({triggerElement: ele_trigger_9, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj9, e.progress.toFixed(3));
      });

    sceneLine10 = new ScrollMagic.Scene({triggerElement: ele_trigger_10, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj10, e.progress.toFixed(3));
      });

    sceneLine11 = new ScrollMagic.Scene({triggerElement: ele_trigger_11, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj11, e.progress.toFixed(3));
      });

    sceneLine12 = new ScrollMagic.Scene({triggerElement: ele_trigger_12, duration: 100})
      .addTo(controller)
      .on("progress", function (e) {
        that.tweenPath(tlObj12, e.progress.toFixed(3));
      });

    that.updateDuration();
  };

  this.animateEle = function () {
    var timeline1 = new TimelineMax()
      .add(TweenMax.fromTo(ele_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');
    var check_popup = true;
    new ScrollMagic.Scene({triggerElement: ele_trigger_1})
      .setTween(timeline1)
      .addTo(controller);
    // .reverse(false)

    var timeline2 = new TimelineMax()
      .add(TweenMax.fromTo(ele_2_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_2_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_2_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_2_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_2_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_2_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_2})
      .setTween(timeline2)
      .addTo(controller);
    // .reverse(false)

    var timeline3 = new TimelineMax()
      .add(TweenMax.fromTo(ele_3_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_3_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_3_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_3_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_3_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_3_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_3})
      .setTween(timeline3)
      .addTo(controller);
    // .reverse(false)

    var timeline4 = new TimelineMax()
      .add(TweenMax.fromTo(ele_4_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_4_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_4_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_4_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_4_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_4_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_4})
      .setTween(timeline4)
      .addTo(controller);
    // .reverse(false)

    var timeline5 = new TimelineMax()
      .add(TweenMax.fromTo(ele_5_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_5_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_5_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_5_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_5_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_5_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_5})
      .setTween(timeline5)
      .addTo(controller);
    // .reverse(false)

    var timeline6 = new TimelineMax()
      .add(TweenMax.fromTo(ele_6_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_6_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_6_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_6_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_6_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_6_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_6})
      .setTween(timeline6)
      .addTo(controller);
    // .reverse(false)

    var timeline7 = new TimelineMax()
      .add(TweenMax.fromTo(ele_7_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_7_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_7_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_7_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_7_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_7_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_7})
      .setTween(timeline7)
      .addTo(controller);
    // .reverse(false)

    var timeline8 = new TimelineMax()
      .add(TweenMax.fromTo(ele_8_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_8_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_8_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_8_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_8_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_8_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_8})
      .setTween(timeline8)
      .addTo(controller);
    // .reverse(false)

    var timeline9 = new TimelineMax()
      .add(TweenMax.fromTo(ele_9_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_9_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_9_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_9_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_9_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_9_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_9})
      .setTween(timeline9)
      .addTo(controller);
    // .reverse(false)

    var timeline10 = new TimelineMax()
      .add(TweenMax.fromTo(ele_10_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_10_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_10_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_10_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_10_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_10_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_10})
      .setTween(timeline10)
      .addTo(controller);
    // .reverse(false)

    var timeline11 = new TimelineMax()
      .add(TweenMax.fromTo(ele_11_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_11_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_11_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_11_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_11_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_11_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_11})
      .setTween(timeline11)
      .addTo(controller);
    // .reverse(false)

    var timeline12 = new TimelineMax()
      .add(TweenMax.fromTo(ele_12_1, 0.5, obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_12_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_12_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_12_2, 0.5, obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_12_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_12_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_12})
      .setTween(timeline12)
      .addTo(controller);
    // .reverse(false)
  };

  this.calcDurationObj = function () {
    var top = $(ele_trigger_0).offset().top;
    var oset1 = $(ele_trigger_1).offset().top;
    var oset2 = $(ele_trigger_2).offset().top;
    var oset3 = $(ele_trigger_3).offset().top;
    var oset4 = $(ele_trigger_4).offset().top;
    var oset5 = $(ele_trigger_5).offset().top;
    var oset6 = $(ele_trigger_6).offset().top;
    var oset7 = $(ele_trigger_7).offset().top;
    var oset8 = $(ele_trigger_8).offset().top;
    var oset9 = $(ele_trigger_9).offset().top;
    var oset10 = $(ele_trigger_10).offset().top;
    var oset11 = $(ele_trigger_11).offset().top;
    var oset12 = $(ele_trigger_12).offset().top;

    durationObj[0] = oset2 - top;
    durationObj[1] = oset3 - oset2;
    durationObj[2] = oset4 - oset3;
    durationObj[3] = oset5 - oset4;
    durationObj[4] = oset6 - oset5;
    durationObj[5] = oset7 - oset6;
    durationObj[6] = oset8 - oset7;
    durationObj[7] = oset9 - oset8;
    durationObj[8] = oset10 - oset9;
    durationObj[9] = oset11 - oset10;
    durationObj[10] = oset12 - oset11;
  };

  this.updateDuration = function () {
    that.calcDurationObj();
    sceneLine1.duration(durationObj[0]);
    sceneLine2.duration(durationObj[1]);
    sceneLine3.duration(durationObj[2]);
    sceneLine4.duration(durationObj[3]);
    sceneLine5.duration(durationObj[4]);
    sceneLine6.duration(durationObj[5]);
    sceneLine7.duration(durationObj[6]);
    sceneLine8.duration(durationObj[7]);
    sceneLine9.duration(durationObj[8]);
    sceneLine10.duration(durationObj[9]);
    sceneLine11.duration(durationObj[10]);
  };

  this.pathPrepare = function($el) {
    var lineLength = $el[0].getTotalLength();
    $el.css("stroke-dashoffset", lineLength);
  };

  this.tweenPath = function (obj, progress) {
    var s = obj.curentlineLength - progress * (obj.curentlineLength - obj.tolineLength);
    TweenMax.killTweensOf(obj);

    // TweenMax.to(path, 0.5,{stroke: '#e72534'});
    TweenMax.to(obj, 1, {
      lineLength: s, ease: Cubic.easeOut,
      onUpdate: function () {
        pathDash.css({"stroke-dashoffset": obj.lineLength});
      },
      onComplete: function () {
        // TweenMax.to(path, 0.5,{stroke: '#1F57A3'});
      }
    });
  };

  this.init = function () {
    wWidth = $(window).width();
    if (wWidth > 1124) {
      flagDevice = 1;
    } else {
      flagDevice = 2;
    }

    that.checkDevice();
    $(window).on('resize', function () {
      that.checkDevice();
    });
  }

  this.checkDevice = function () {
    wWidth = $(window).width();
    if (wWidth > 1124 && flagDevice == 1) {
      flagDevice = 2;
      that.startController();
      if (!that.checkSafari()) {
        that.updateDuration();
      }
    }

    if (wWidth < 1124 && flagDevice == 2) {
      flagDevice = 1;
      if (typeof controller !== "undefined") {
        controller.destroy(true);
        controller = null;
      }
      $('.article .img, .article .text .content, .article .text .view-detail, .article .text .title img').attr('style', '');
    }
  }

  this.checkSafari = function () {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf('safari') != -1) {
      if (ua.indexOf('chrome') > -1) {
        return false;
      } else {
        return false;
      }
      return false;
    }
  };

  this.startController = function () {
    that.pathPrepare(pathDash);
    controller = new ScrollMagic.Controller();
    if (that.checkSafari()) {
      pathDash.css({'stroke-dashoffset': 0});
    } else {
      that.animateLine();
    }
    that.animateEle();
  };

  this.preload = function (image) {
    if (typeof document.body == "undefined") return;
    try {
      var div = document.createElement("div");
      var s = div.style;
      s.position = "absolute";
      s.top = s.left = 0;
      s.width = s.height = 0;
      s.overflow = "hidden";
      s.visibility = "hidden";
      document.body.appendChild(div);
      div.innerHTML = "<img class=\"preload_img\" src=\"" + image + "\" />";
    } catch (e) {

    }
  };

  this.loadingImg = function () {
    var totalImg = $('#landing-content img').length;
    var index = 0;

    $('#landing-content img').each(function () {
      var image = $(this).attr('src');
      that.preload(image);
    });

    $('.preload_img').load(function () {
      index++;
      if (index == totalImg) {
        that.init();
      }
    });
  }
}

var homeAnimate = new homeAnimate();
$(document).ready(function () {
  homeAnimate.loadingImg();
});