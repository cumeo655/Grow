$(document).ready(function () {
  $("#button").click(function () {
    $('html, body').animate({
      scrollTop: $("#myDiv").offset().top
    }, 2000);
  });

  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    vertical: true,
    verticalSwiping: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: false,
    focusOnSelect: true
  });

  $('.share-detail').slimScroll({
    height: '120px'
  });

  //Setup wow js
  var wow = new WOW({
    boxClass: 'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 0,          // distance to the element when triggering the animation (default is 0)
    mobile: true,       // trigger animations on mobile devices (default is true)
    live: true,       // act on asynchronously loaded content (default is true)
    callback: function (box) {
      // the callback is fired every time an animation is started
      // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
  });
  wow.init();

  $('.scroll-block').slimScroll({
    height: '300px',
    alwaysVisible: true
  });

  $('.left .detail-tree').slimScroll({
    height: 'auto',
    alwaysVisible: true
  });

  $('.detail-tree-mobile').slimScroll({
    height: 'auto',
    alwaysVisible: true
  });

  $('.detail-topic').slimScroll({
    height: '200px',
    alwaysVisible: true
  });

  $('.right .detail-tree').slimScroll({
    height: 'auto',
    alwaysVisible: true
  });

  $('.tree-scroll').slimScroll({
    height: 'auto',
    alwaysVisible: true
  });

  $('.scroll-block').slimScroll({
    height: 'auto',
    alwaysVisible: true
  });

  $('#story-list').owlCarousel({
    slideSpeed: 300,
    items: 4,
    loop: true,
    margin: 10,
    pagination: false,
    dots: false,
    nav: true,
    navText: [
      "<i class='icon prev-icon'></i>",
      "<i class='icon next-icon'></i>"
    ],
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      768: {
        items: 2,
        nav: false
      },
      992: {
        items: 3,
        nav: false
      },
      1200: {
        items: 4,
        nav: true
      }
    },
    autoplay: true,
    autoplayHoverPause: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 2000
  });

  $('#success-list').owlCarousel({
    slideSpeed: 300,
    items: 2,
    loop: true,
    margin: 10,
    pagination: false,
    dots: false,
    nav: true,
    navText: [
      "<i class='icon prev-icon'></i>",
      "<i class='icon next-icon'></i>"
    ],
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      768: {
        items: 2,
        nav: false
      }
    },
    autoplay: true,
    autoplayHoverPause: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 2000
  });

  //check if browser supports file api and filereader features
  if (window.File && window.FileReader && window.FileList && window.Blob) {

    //this function is called when the input loads an image
    function renderImage(file) {
      var reader = new FileReader();
      reader.onload = function (event) {
        var the_url = event.target.result;

        if (file.type.match('image.*')) {
          $('#holder').addClass('holder-hover').find('.holder-image').html("<img src='" + the_url + "' />");
          $('#holder').find('.avatar').css('opacity', '0');
        } else {
          alert('File n�y ko ph?i l� ?nh.');
        }
      };

      //when the file is read it triggers the onload event above.
      reader.readAsDataURL(file);
    }

    //watch for change on the
    $("#upload-photo").change(function () {
      //grab the first image in the fileList
      //in this example we are only loading one file.
      renderImage(this.files[0]);

    });
  } else {
    alert('The File APIs are not fully supported in this browser.');
  }

  $('#holder').on('click', function (e) {
    e.preventDefault();
    $("#upload-photo").trigger('click');
  });

  $('.slider-news').slick({
    centerPadding: 0,
    slidesToShow: 1,
    infinite: true,
    dots: true,
    speed: 300,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000
  });

  $('.center').slick({
    centerMode: true,
    centerPadding: 0,
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerPadding: '80px',
          centerMode: true,
          slidesToShow: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerPadding: '40px',
          centerMode: true,
          slidesToShow: 1
        }
      }
    ]
  });

  $('.center-pd').slick({
    centerMode: true,
    centerPadding: 0,
    slidesToShow: 3,
    arrows: true,
    responsive: [
      {
        breakpoint: 1180,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: 0,
          slidesToShow: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: 0,
          slidesToShow: 1
        }
      }
    ]
  });

  $('.data').slick(
    {
      responsive: [
        {
          breakpoint: 1280,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 1180,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1
          }
        }]
    });

  $('.data-home').slick();

  $('ul.nav-nutifood li.dropdown').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
  }, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
  });

  jQuery.noConflict();
  var Modul = (function ($, w) //shortcuts $=jQuery, w=window
  {
    var APP = {
      init: function () {
        //start methods within APP
        APP.submenu.init();
      },
      submenu: {
        init: function () {
          APP.submenu.subToggleListener()
          APP.submenu.dropDownListener()
        },
        $allSubMenus: $('.subdown'), // all submenu li's

        // Listen for click on each .subdown > a.sub-toggle
        subToggleListener: function () {
          APP.submenu.$allSubMenus.on('click', '.sub-toggle', function (eve) {
            $this = $(this)
            // Hide submenus that aren't parrents of clicked .sub-toggle
            // Select all .submenu.open
            var openSubmenus = $('.subdown.open').get()
            // Select all .submenu.open ancestors of the clicked toggle
            var $ancestorsOfClickedToggle = $this.parents('.subdown.open').get()

            // Go through openSubmenus
            for (var i = 0, ii = openSubmenus.length; i < ii; i++) {
              // Isn't current openSubmenus[i] in $ancestorsOfClickedToggle?
              if ($ancestorsOfClickedToggle.indexOf(openSubmenus[i]) === -1) {
                // If it isn't, remove .open class
                $(openSubmenus[i]).removeClass('open')
              }
              // If it is present, ignore it.
            }

            // when a.sub-toggle clicked >> toggle .open to closest('.subdown')
            $this.closest('.subdown')
              .toggleClass('open')

            // Prevent default click event
            eve.preventDefault()
            return false;
          })
        },
        dropDownListener: function () {
          // Clean up open subdown menus
          $('.dropdown-toggle').on('click', function (eve) {
            $('.subdown.open').removeClass('open')
          })
        }
      } //end of Submenu
    };
    $(document).ready(function () {
      APP.init();
    });
  })(jQuery, window, undefined);
});
