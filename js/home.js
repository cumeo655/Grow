function homeAnimate(){
  var that = this;
  var wWidth, wHeight, flagDevice = 1;
  var pathDash = $('.dashed'),
    durationObj = [];
  var controller;
  var sceneLine1, sceneLine2, sceneLine3;

  var tlObj1 = {
      lineLength: 10780,
      curentlineLength: 10780,
      tolineLength: 8500,
      time: 3
    },
    tlObj2 = {
      lineLength: 10780,
      curentlineLength: 8500,
      tolineLength: 7160,
      time: 3
    },
    tlObj3 = {
      lineLength: 10780,
      curentlineLength: 7160,
      tolineLength: 5615,
      time: 3
    };

  var ele_trigger_0 = '#home-content',
    ele_trigger_1 = '#home-article-1 .triggerAnimate',
    ele_trigger_2 = '#home-article-2 .triggerAnimate',
    ele_trigger_3 = '#home-article-3 .triggerAnimate';

  var ele_1 = '#home-article-1 .img',
    ele_2 = '#home-article-1 .title .main-title',
    ele_title_2 = '#home-article-1 .title .title-content',
    ele_sub_title_2 = '#home-article-1 .title .sub-content',
    ele_3 = '#home-article-1 .content',
    ele_4 = '#home-article-1 .view-detail';

  var ele_2_1 = '#home-article-2 .img',
    ele_2_2 = '#home-article-2 .title .main-title',
    ele_title_2_2 = '#home-article-2 .title .title-content',
    ele_sub_title_2_2 = '#home-article-2 .title .sub-content',
    ele_2_3 = '#home-article-2 .content',
    ele_2_4 = '#home-article-2 .view-detail';

  var ele_3_1 = '#home-article-3 .img',
    ele_3_2 = '#home-article-3 .title .main-title',
    ele_title_3_2 = '#home-article-3 .title .title-content',
    ele_sub_title_3_2 = '#home-article-3 .title .sub-content',
    ele_3_3 = '#home-article-3 .content',
    ele_3_4 = '#home-article-3 .view-detail';

  var obj_ant = [
    {opacity: 0},
    {opacity: 1, ease: Sine.easeOut},
    {opacity: 1, ease: Back.easeOut},
    {opacity: 0, x: 100},
    {opacity: 1, x: 0, ease: Quint.easeOut}
  ];

  this.animateLine = function(){
    sceneLine1 = new ScrollMagic.Scene({triggerElement: ele_trigger_0, duration: 100})
      .addTo(controller)
      .on("progress", function(e){
        that.tweenPath(tlObj1, e.progress.toFixed(3));
      });

    sceneLine2 = new ScrollMagic.Scene({triggerElement: ele_trigger_2, duration: 100})
      .addTo(controller)
      .on("progress", function(e){
        that.tweenPath(tlObj2, e.progress.toFixed(3));
      });

    sceneLine3 = new ScrollMagic.Scene({triggerElement: ele_trigger_3, duration: 100})
      .addTo(controller)
      .on("progress", function(e){
        that.tweenPath(tlObj3, e.progress.toFixed(3));
      });

    that.updateDuration();
  };

  this.animateEle = function(){
    var timeline1 = new TimelineMax()
      .add(TweenMax.fromTo(ele_1, 0.5,obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_2, 0.5,obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_2, 0.5,obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_2, 0.5,obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');
    var check_popup = true;
    new ScrollMagic.Scene({triggerElement: ele_trigger_1})
      .setTween(timeline1)
      .addTo(controller);
    // .reverse(false)

    var timeline2 = new TimelineMax()
      .add(TweenMax.fromTo(ele_2_1, 0.5,obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_2_2, 0.5,obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_2_2, 0.5,obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_2_2, 0.5,obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_2_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_2_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_2})
      .setTween(timeline2)
      .addTo(controller);
    // .reverse(false)

    var timeline3 = new TimelineMax()
      .add(TweenMax.fromTo(ele_3_1, 0.5,obj_ant[0], obj_ant[1]))
      .add(TweenMax.fromTo(ele_3_2, 0.5,obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_title_3_2, 0.5,obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_sub_title_3_2, 0.5,obj_ant[0], obj_ant[2]), '-=0.3')
      .add(TweenMax.fromTo(ele_3_3, 0.5, obj_ant[3], obj_ant[4]), '-=0.3')
      .add(TweenMax.fromTo(ele_3_4, 0.5, obj_ant[3], obj_ant[4]), '-=0.3');

    new ScrollMagic.Scene({triggerElement: ele_trigger_3})
      .setTween(timeline3)
      .addTo(controller);
    // .reverse(false)
  };

  this.calcDurationObj = function(){
    var top = $(ele_trigger_0).offset().top;
    var oset1 = $(ele_trigger_1).offset().top;
    var oset2 = $(ele_trigger_2).offset().top;
    var oset3 = $(ele_trigger_3).offset().top;

    durationObj[0] =  oset2 - top;
    durationObj[1] =  oset3 - oset2;
  };

  this.updateDuration = function(){
    that.calcDurationObj();
    sceneLine1.duration(durationObj[0]);
    sceneLine2.duration(durationObj[1]);
  };

  this.pathPrepare = function($el) {
    var lineLength = $el[0].getTotalLength();
    $el.css("stroke-dashoffset", lineLength);
  };

  this.tweenPath = function(obj, progress){
    var s = obj.curentlineLength - progress * (obj.curentlineLength - obj.tolineLength);
    TweenMax.killTweensOf(obj);

    // TweenMax.to(path, 0.5,{stroke: '#e72534'});
    TweenMax.to(obj, 1, {lineLength: s, ease: Cubic.easeOut,
      onUpdate:function(){
        pathDash.css({"stroke-dashoffset": obj.lineLength});
      },
      onComplete: function(){
        // TweenMax.to(path, 0.5,{stroke: '#1F57A3'});
      }
    });
  };

  this.init = function(){
    wWidth = $(window).width();
    if(wWidth > 1025){
      flagDevice = 1;
    }else{
      flagDevice = 2;
    }

    that.checkDevice();
    $(window).on('resize', function(){
      that.checkDevice();
    });
  }

  this.checkDevice = function(){
    wWidth = $(window).width();
    if(wWidth > 1025 && flagDevice == 1){
      flagDevice = 2;
      that.startController();
      if(!that.checkSafari()){
        that.updateDuration();
      }
    }

    if(wWidth < 1025 && flagDevice == 2){
      flagDevice = 1;
      if(typeof controller !== "undefined"){
        controller.destroy(true);
        controller = null;
      }
      $('.article .img, .article .text .content, .article .text .view-detail, .article .text .title img').attr('style', '');
    }
  }

  this.checkSafari = function(){
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf('safari') != -1) {
      if (ua.indexOf('chrome') > -1) {
        return false;
      } else {
        return false;
      }
      return false;
    }
  };

  this.startController = function(){
    that.pathPrepare(pathDash);
    controller = new ScrollMagic.Controller();
    if(that.checkSafari()){
      pathDash.css({'stroke-dashoffset': 0});
    }else{
      that.animateLine();
    }
    that.animateEle();
  };

  this.preload = function(image) {
    if (typeof document.body == "undefined") return;
    try {
      var div = document.createElement("div");
      var s = div.style;
      s.position = "absolute";
      s.top = s.left = 0;
      s.width = s.height = 0;
      s.overflow = "hidden";
      s.visibility = "hidden";
      document.body.appendChild(div);
      div.innerHTML = "<img class=\"preload_img\" src=\"" + image + "\" />";
    } catch(e) {

    }
  };

  this.loadingImg = function(){
    var totalImg = $('#home-content img').length;
    var index = 0;

    $('#home-content img').each(function(){
      var image = $(this).attr('src');
      that.preload(image);
    });

    $('.preload_img').load(function() {
      index++;
      if(index == totalImg){
        that.init();
      }
    });
  }
}

var homeAnimate = new homeAnimate();
$(document).ready(function(){
  homeAnimate.loadingImg();
});